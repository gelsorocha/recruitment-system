from recruitment_system.domain.job import Job
from recruitment_system.domain.person import Person
from recruitment_system.domain.candidature import Candidature


class BaseRepository:
    def __init__(self, session):
        self.session = session

    def add(self, info):
        self.session.add(info)
        self.session.commit()


class JobRepository(BaseRepository):
    def get(self, id):
        return self.session.query(Job).get(id)

    def list(self):
        return self.session.query(Job).all()


class PersonRepository(BaseRepository):
    def get(self, id):
        return self.session.query(Person).get(id)

    def list(self):
        return self.session.query(Person).all()


class CandidatureRepository(BaseRepository):
    def list(self):
        return self.session.query(Candidature).all()
