from dataclasses import dataclass
from recruitment_system.domain.enums import LevelEnum, LocalizationEnum


@dataclass
class Person:
    name: str
    profission: str
    localization: LocalizationEnum
    level: LevelEnum

    @staticmethod
    def person_from_request(nome: str, profissao: str, localizacao: str, nivel: str):
        return Person(name=nome, profission=profissao, localization=localizacao, level=nivel)
