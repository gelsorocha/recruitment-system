from recruitment_system.domain.graph import Graph, Adjacency
import sys


class Dijkstra:
    def __init__(self, graph: Graph):
        self.graph = graph
        self.distances = [sys.maxsize for x in range(0, graph.vertex_len)]
        self.previous = [-1 for x in range(0, graph.vertex_len)]
        self.open = [True for x in range(0, graph.vertex_len)]

    def relax(self, unity: int, vertex: int):
        adjacency = self.graph.adjacency[unity].head

        while (adjacency and adjacency.vertex != vertex):
                adjacency = adjacency.next_adjacency
        if adjacency:
            if self.distances[vertex] > (self.distances[unity] + adjacency.weigth):
                self.distances[vertex] = self.distances[unity] + adjacency.weigth
                self.previous[vertex] = unity


    def has_open_vertex(self) -> bool:
        return any(self.open)


    def get_shortest_distance(self) -> int:
        for i in range(0, self.graph.vertex_len):
            if self.open[i]:
                break
        if (i == self.graph.vertex_len):
            return -1
        shortest = i

        for x in range(shortest+1, self.graph.vertex_len):
            if (self.open[x] and self.distances[shortest] > self.distances[x]):
                shortest = x
        return shortest



    def apply_dijkstra(self, initial_vertex: int):
        self.distances[initial_vertex] = 0
        while self.has_open_vertex():
            unity = self.get_shortest_distance()
            self.open[unity] = False
            adjacency = self.graph.adjacency[unity].head

            while (adjacency):
                self.relax(unity, adjacency.vertex)
                adjacency = adjacency.next_adjacency
