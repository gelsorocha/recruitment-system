from enum import Enum


class LevelEnum(Enum):
    TRAINEE = 1
    JUNIOR = 2
    FULL = 3
    SENIOR = 4
    SPECIALIST = 5


class LocalizationEnum(Enum):
    A = 0
    B = 1
    C = 2
    D = 3
    E = 4
    F = 5
