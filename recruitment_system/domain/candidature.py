from dataclasses import dataclass
from recruitment_system.domain.job import Job
from recruitment_system.domain.person import Person
from recruitment_system.domain.graph import Graph
from recruitment_system.domain.dijkstra import Dijkstra
from recruitment_system.domain.enums import LocalizationEnum


class RecruitmentGraph:
    def __init__(self):
        self.graph = Graph(6)

    def initialize(self):
        self.graph.create_edge_undirected(0, 1, 5)
        self.graph.create_edge_undirected(1, 2, 7)
        self.graph.create_edge_undirected(1, 3, 3)
        self.graph.create_edge_undirected(2, 4, 4)
        self.graph.create_edge_undirected(3, 4, 10)
        self.graph.create_edge_undirected(3, 5, 8)

    def get_distance(self, localization_a: LocalizationEnum, localization_b: LocalizationEnum):
        dja = Dijkstra(self.graph)
        dja.apply_dijkstra(localization_a.value)

        return dja.distances[localization_b.value]



class Candidature:
    def __init__(self, job: Job, person: Person):
        self.job = job
        self.person = person
        self.rec_graph = RecruitmentGraph()
        self.rec_graph.initialize()

    @property
    def d(self):
        x = self.rec_graph.get_distance(self.person.localization, self.job.localization)

        if x >= 0 and x <= 5:
            return 100

        if x >= 6 and x <= 10:
            return 75

        if x >= 11 and x <= 15:
            return 50

        if x >= 16 and x <= 20:
            return 25

        return 0

    @property
    def score(self):
        score = ((100 - 25 * abs(self.job.level.value - self.person.level.value)) + self.d) / 2
        return int(score)
