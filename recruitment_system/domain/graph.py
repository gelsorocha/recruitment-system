from dataclasses import dataclass
from typing import List, Any


@dataclass
class Adjacency:
    weigth: int
    vertex: int
    next_adjacency: Any = None


@dataclass
class Edge:
    head: Adjacency


class Graph:
    def __init__(self, vertex_len: int):
        self.vertex_len = vertex_len
        self.edges_len = 0
        self.adjacency = [Edge(None) for x in range(0, vertex_len)]

    @staticmethod
    def create_adjcency(vertex: int, weigth: int):
        return Adjacency(weigth, vertex)

    def create_edge_undirected(self, initial_vertex: int, final_vertex: int, weigth: int):
        self.create_edge(initial_vertex, final_vertex, weigth)
        self.create_edge(final_vertex, initial_vertex, weigth)

    def create_edge(self, initial_vertex: int, final_vertex: int, weigth: int):
        new = Graph.create_adjcency(final_vertex, weigth)
        new.next_adjacency = self.adjacency[initial_vertex].head
        self.adjacency[initial_vertex].head = new
        self.edges_len+=1
