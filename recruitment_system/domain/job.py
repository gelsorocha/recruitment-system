from dataclasses import dataclass
from recruitment_system.domain.enums import LevelEnum, LocalizationEnum


@dataclass
class Job:
    employer: str
    title: str
    description: str
    localization: LocalizationEnum
    level: LevelEnum


    @staticmethod
    def job_from_request(empresa: str, titulo: str, descricao: str, localizacao: str, nivel: str):
        return Job(employer=empresa, title=titulo, description=descricao, localization=localizacao, level=nivel)
