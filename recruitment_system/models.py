from sqlalchemy import Table, MetaData, Column, Integer, String, ForeignKey
from sqlalchemy.orm import mapper

from recruitment_system.domain.job import Job
from recruitment_system.domain.person import Person
from recruitment_system.domain.candidature import Candidature

metadata = MetaData()

job = Table('job', metadata,
            Column('id', Integer, primary_key=True),
            Column('employer', String(50)),
            Column('title', String(50)),
            Column('description', String(100)),
            Column('localization', String(100)),
            Column('level', String(100)),
            )


person = Table('person', metadata,
            Column('id', Integer, primary_key=True),
            Column('name', String(50)),
            Column('profission', String(100)),
            Column('localization', String(100)),
            Column('level', String(100)),
            )


candidature = Table('candidature', metadata,
            Column('id', Integer, primary_key=True),
            Column('person_id', Integer, ForeignKey('person.id')),
            Column('job_id', Integer, ForeignKey('job.id')),
            )


mapper(Job, job)
mapper(Person, person)
mapper(Candidature, candidature)
