FROM python:3.9.2-buster
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pip install mysqlclient
RUN python -m pip install -r requirements.txt
RUN python -m pip install -e .
EXPOSE 5000
CMD ["python", "rest/api.py"]
