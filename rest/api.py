from flask import Flask
from flask_restful import Resource, Api, reqparse, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy

from dotenv import load_dotenv
import os


from recruitment_system.repository import JobRepository, PersonRepository, CandidatureRepository

from recruitment_system.domain.job import Job
from recruitment_system.domain.person import Person
from recruitment_system.domain.candidature import Candidature
import recruitment_system.models
from recruitment_system.domain.enums import LevelEnum, LocalizationEnum

load_dotenv()

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URI')

api = Api(app, '/v1')
db = SQLAlchemy(app)


def level(value):
    if value not in [item.value for item in LevelEnum]:
        raise ValueError('Nivel invalido')
    return value

def localization(value):
    if value not in LocalizationEnum.__members__:
        raise ValueError('Localizacao invalida')
    return value

base_parser = reqparse.RequestParser()
base_parser.add_argument('localizacao', type=localization)
base_parser.add_argument('nivel', type=level)

job_parser = base_parser.copy()
job_parser.add_argument('empresa')
job_parser.add_argument('titulo')
job_parser.add_argument('descricao')


person_parser = base_parser.copy()
person_parser.add_argument('nome')
person_parser.add_argument('profissao')


candidature_parser = reqparse.RequestParser()
candidature_parser.add_argument('id_pessoa')
candidature_parser.add_argument('id_vaga')

cadidature_fields = {
        "score": fields.String
        }



job_repository = JobRepository(db.session)
person_repository = PersonRepository(db.session)
candidature_repository = CandidatureRepository(db.session)

class JobResource(Resource):
    def post(self):
        args = job_parser.parse_args()
        job_repository.add(Job.job_from_request(**args))
        return args


class PersonResource(Resource):
    def post(self):
        args = person_parser.parse_args()
        person_repository.add(Person.person_from_request(**args))
        return args


class CandidatureResource(Resource):
    def post(self):
        args = candidature_parser.parse_args()
        job = job_repository.get(args['id_vaga'])
        person = person_repository.get(args['id_pessoa'])
        candidature_repository.add(Candidature(job=job, person=person))
        return args

    @marshal_with(cadidature_fields)
    def get(self):
        return candidature_repository.list()

api.add_resource(JobResource, '/vagas')
api.add_resource(PersonResource, '/pessoas')
api.add_resource(CandidatureResource, '/candidaturas')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
