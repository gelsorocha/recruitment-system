import pytest

from recruitment_system.domain.person import Person
from recruitment_system.domain.job import Job
from recruitment_system.domain.enums import LevelEnum, LocalizationEnum


@pytest.fixture
def person_stub():
    return Person('batima', 'trabalhador', LocalizationEnum.B, LevelEnum.SENIOR)


@pytest.fixture
def job_stub():
    return Job('empregador', 'trabalho', 'trabalhe', LocalizationEnum.A, LevelEnum.TRAINEE)
