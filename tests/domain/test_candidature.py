from recruitment_system.domain.candidature import Candidature


class TestCandidature:
    def test_calculate_score(self, person_stub, job_stub):
        candidature = Candidature(job=job_stub, person=person_stub)

        assert candidature.score == 62
