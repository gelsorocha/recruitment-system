from recruitment_system.domain.graph import Graph
from recruitment_system.domain.dijkstra import Dijkstra


class TestGraphAndShortestPath:
    def test_study_graph(self):
        graph = Graph(vertex_len=6)

        graph.create_edge(0, 1, 10)
        graph.create_edge(0, 2, 5)

        graph.create_edge(2, 1, 3)
        graph.create_edge(1, 3, 1)
        graph.create_edge(2, 3, 8)
        graph.create_edge(2, 4, 2)
        graph.create_edge(4, 5, 6)
        graph.create_edge(3, 5, 4)
        graph.create_edge(3, 4, 4)

        dja = Dijkstra(graph)

        dja.apply_dijkstra(0)


        assert dja.distances == [0, 8, 5, 9, 7, 13]


    def test_recruitment_graph(self):
        graph = Graph(6)

        graph.create_edge_undirected(0, 1, 5)
        graph.create_edge_undirected(1, 2, 7)
        graph.create_edge_undirected(1, 3, 3)
        graph.create_edge_undirected(2, 4, 4)
        graph.create_edge_undirected(3, 4, 10)
        graph.create_edge_undirected(3, 5, 8)

        dja = Dijkstra(graph)
        dja.apply_dijkstra(0)

        assert dja.distances == [0, 5, 12, 8, 16, 16]

        dja2 = Dijkstra(graph)
        dja2.apply_dijkstra(3)

        assert dja2.distances == [8, 3, 10, 0, 10, 8]
